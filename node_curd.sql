-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2020 at 03:30 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `node_curd`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`user_id`, `name`, `email`, `password`) VALUES
(6, 'Sathya', 'sathiyanarayanan@techmango.net', '123456'),
(7, 'Mugesh', 'mugesh@techmango.net', '123456'),
(8, 'Mani', 'mani@techmango.net', '123456'),
(9, 'sdfsdfsd', 'safaf@sdfds.sdf', 'adfadf'),
(10, 'dfg', 'dfg@sf.sdf', 'dfgdfg'),
(11, 'dfg', 'dfg@sf.sdf', 'dfgdfg'),
(12, 'dfg', 'dfg@sf.sdf', 'dfgdfg'),
(13, 'asdfas', 'adsfa@sdf.sdfsd', 'asdfadf'),
(14, 'asdfas', 'adsfa@sdf.sdfsd', 'asdfadf'),
(15, 'dfg', 'dfgdfg@sdf.sdf', 'adsfas'),
(16, 'dfg', 'dfgdfg@sdf.sdf', 'adsfas'),
(17, 'dfg', 'dfgdfg@sdf.sdf', 'adsfas'),
(18, 'dfg', 'dfgdfg@sdf.sdf', 'adsfas'),
(19, 'dfgfd', 'dfg@sf.sdf', 'dfgdfg'),
(20, 'sdfdsf', 'sdf@sdf.sdf', 'sdfsdf'),
(21, 'TOYOTO', 'sdf@sdf.sdf', 'sdfdsfd'),
(22, 'TOYOTO', 'sdf@sdf.sdf', 'sdfdsfd'),
(23, 'dating', 'demouser@gmail.com', 'sdfsdf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
