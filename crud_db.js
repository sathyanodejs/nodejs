var express  = require('express'),
    path     = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator'),
    expressSession     = require('express-session'),
    multer = require("multer");

    


/*Set EJS template Engine*/
app.set('views','./views');
app.set('view engine','ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());
app.use(expressValidator());

// set session options in index.js 
app.use(expressSession({
    secret: '123456',
    resave: true,
    saveUninitialized: true,
    key: 'user_id',
    cookie: { maxAge: 86400 * 30 } //  24 * 60 * 60 * 1000 (24 hours) // micro seconds
}));
var ssn;


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, __dirname+'/uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
    }
  })
   
  var upload = multer({ storage: storage });

/*MySql connection*/
var connection  = require('express-myconnection'),
    mysql = require('mysql');

app.use(

    connection(mysql,{
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'node_curd',
        debug    : false //set true if you wanna see debug logger
    },'request')

);

app.get('/',function(req,res){
    ssn = req.session;
    if(ssn.email){
        return res.redirect('/');
    } else {
        return res.render('login',{title:"User Login"});
    }
});

app.get('/register',function(req,res){
    ssn = req.session;
    if(ssn.email){
        return res.redirect('/');
    } else {
        return res.render('register',{title:"User Registration"});
    }
});

app.get('/logout',function(req,res){
    req.session.destroy();
    return res.redirect('/');
});


//RESTful route
var router = express.Router();



router.use(function(req, res, next) {
    console.log(req.method, req.url);
    next();
});

var routeLogin = router.route('/login');

routeLogin.post(function(req,res,next){
    req.assert('email','A valid email is required').isEmail();
    req.assert('password','Enter a password 6 - 20').len(6,20);

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }
    var email = req.body.email;
    var password = req.body.password;
    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM t_user WHERE email = ? AND password = ? ",[email,password],function(err,rows){
            //console.log(rows[0].user_id);
            
            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }
            
            if(rows.length > 0) {
                ssn = req.session;
                ssn.user_id = rows[0].user_id;
                ssn.name = rows[0].name;
                ssn.email = rows[0].email;
                //req.session.qapuser = { user_id: rows[0].user_id, name : rows[0].name , email: rows[0].email }  
                // check and clear session cookie key values 
               /* app.use((req, res, next) => {
                    if (req.cookies.user_id && !req.session.qapuser ) {
                        res.clearCookie('user_id');
                    }
                    next();
                }); */
               res.sendStatus(200);
            } else {
                return res.send("User Not found");
            }
        });

    });

});

var route1 = router.route('/user');


//show the CRUD interface | GET
route1.get(function(req,res,next){
    ssn = req.session;
    if(ssn.email){
        req.getConnection(function(err,conn){

            if (err) return next("Cannot Connect");
    
            var query = conn.query('SELECT * FROM t_user',function(err,rows){
    
                if(err){
                    console.log(err);
                    return next("Mysql error, check your query");
                }
    
                res.render('user',{title:"CRUD POC",data:rows});
    
             });
    
        });
    } else {
        return res.redirect('/');
    }
    

});
//post data to DB | POST
//route1.post('/user', upload.single('myfile'), (req, res, next) => {
route1.post(function(req,res,next){
    //validation
    req.assert('name','Name is required').notEmpty();
    req.assert('email','A valid email is required').isEmail();
    req.assert('password','Enter a password 6 - 20').len(6,20);
    console.log(req.body)

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        name:req.body.name,
        email:req.body.email,
        password:req.body.password
     };

    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("INSERT INTO t_user set ? ",data, function(err, rows){

           if(err){
                console.log(err);
                return next("Mysql error, check your query");
           }
          res.sendStatus(200);

        });

     });

});


//now for Single route (GET,DELETE,PUT)
var route = router.route('/user/:user_id');


route.all(function(req,res,next){
    console.log("List All Users");
    console.log(req.params);
    next();
});

//get data to update
route.get(function(req,res,next){

    var user_id = req.params.user_id;

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM t_user WHERE user_id = ? ",[user_id],function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            //if user not found
            if(rows.length < 1)
                return res.send("User Not found");

            res.render('edit',{title:"Edit user",data:rows});
        });

    });

});

//update data
route.put(function(req,res,next){
    var user_id = req.params.user_id;

    //validation
    req.assert('name','Name is required').notEmpty();
    req.assert('email','A valid email is required').isEmail();
    req.assert('password','Enter a password 6 - 20').len(6,20);

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }

    //get data
    var data = {
        name:req.body.name,
        email:req.body.email,
        password:req.body.password
     };

    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("UPDATE t_user set ? WHERE user_id = ? ",[data,user_id], function(err, rows){

           if(err){
                console.log(err);
                return next("Mysql error, check your query");
           }

          res.sendStatus(200);

        });

     });

});

//delete data
route.delete(function(req,res,next){

    var user_id = req.params.user_id;

     req.getConnection(function (err, conn) {

        if (err) return next("Cannot Connect");

        var query = conn.query("DELETE FROM t_user  WHERE user_id = ? ",[user_id], function(err, rows){

             if(err){
                console.log(err);
                return next("Mysql error, check your query");
             }

             res.sendStatus(200);

        });
        //console.log(query.sql);

     });
});

//now we need to apply our router here
app.use('/', router);

//start Server
var server = app.listen(2020,function(){

   console.log("Listening to port %s",server.address().port);

});
